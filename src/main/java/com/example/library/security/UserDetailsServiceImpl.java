package com.example.library.security;

import com.example.library.model.User;
import com.example.library.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
        User user;

        if (userEmail.contains("@")) {
            user = userRepository.findByEmail(userEmail).orElseThrow(() -> new UsernameNotFoundException("User Not Found with : " + userEmail));
        } else {
            user = userRepository.findByUsername(userEmail)
                    .orElseThrow(() -> new UsernameNotFoundException("User Not Found with : " + userEmail));
        }

        return UserDetailsImpl.build(user);
    }

    public Optional<User> getFaqsByID(UUID id) {
        return userRepository.findById(id);
    }

}