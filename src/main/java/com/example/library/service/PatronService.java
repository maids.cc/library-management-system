package com.example.library.service;

import com.example.library.exceptions.RecordNotFoundException;
import com.example.library.model.Patron;
import com.example.library.repo.PatronRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatronService {

    @Autowired
    private PatronRepo patronRepo;

    public List<Patron> getAllPatrons() {
        return patronRepo.findAll();
    }

    public Patron getPatronByID(Long id) {
        return patronRepo.findById(id).orElseThrow(() -> new RecordNotFoundException(" this Patron not found : " + id));
    }

    public Patron addPatron(Patron newPatron) {
        return patronRepo.save(newPatron);
    }

    public Patron updatePatron(Patron newPatron) {
        return patronRepo.save(newPatron);
    }

    public void deletePatron(Long id) {
        patronRepo.deleteById(id);

    }
}
