package com.example.library.service;

import com.example.library.exceptions.RecordNotFoundException;
import com.example.library.model.BorrowingRecord;
import com.example.library.repo.BorrowingRecordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BorrowingRecordService {

    @Autowired
    private  BorrowingRecordRepo borrowingRecordRepo;



    public BorrowingRecord getBorrowingRecordByID(Long id) {
        return borrowingRecordRepo.findById(id).orElseThrow(() -> new RecordNotFoundException(" this BorrowingRecord not found : " + id));
    }

    public BorrowingRecord addBorrowingRecord(BorrowingRecord newBook) {
        return borrowingRecordRepo.save(newBook);
    }

    public BorrowingRecord updateBorrowingRecord(BorrowingRecord newBook) {
        return borrowingRecordRepo.save(newBook);
    }


    public  boolean checkBorrowBookAvailable(Long bookId) {
        try {

            Optional<BorrowingRecord> record = borrowingRecordRepo.getRecordsByBookId(bookId);
            if (record.isEmpty()) // check if the book is not exist before in borrowing records.
            {
                System.out.println("1");

                return true; //then make borrow available;
            }
            else
            {
                System.out.println("2");
                // get the last record for book ID and check
                // if returned data null =>  borrow not available
                // else if returned date then this record updated and book is returned  =>  borrow available
                Object lastReturnedDateForBook = borrowingRecordRepo.getLastRecordForBookId(bookId);
                System.out.println(lastReturnedDateForBook);
                // book not returned to library yet.
            if (lastReturnedDateForBook == null) {
                System.out.println("3");
                return false;

            }
            else
            {
                System.out.println("4");
                return true;
            }
            }
        }catch (Exception e) {
            System.out.println("5");
            System.out.println(e.getMessage());
        }
        return false;
    }

}
