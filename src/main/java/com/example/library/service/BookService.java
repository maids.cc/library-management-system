package com.example.library.service;

import com.example.library.exceptions.RecordNotFoundException;
import com.example.library.model.Book;
import com.example.library.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    private BookRepo bookRepo;

    public List<Book> getAllBooks() {
        return bookRepo.findAll();
    }

    public Book getBookByID(Long id) {
        return bookRepo.findById(id).orElseThrow(() -> new RecordNotFoundException(" this Book not found : " + id));
    }

    public Book addBook(Book newBook) {
        return bookRepo.save(newBook);
    }

    public Book updateBook(Book newBook) {
        return bookRepo.save(newBook);
    }

    public void deleteBook(Long id) {
        bookRepo.deleteById(id);

    }
}