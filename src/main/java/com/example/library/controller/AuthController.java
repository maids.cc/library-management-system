package com.example.library.controller;

import com.example.library.allResponse.Response;
import com.example.library.constants.RoleType;
import com.example.library.dto.request.LoginRequest;
import com.example.library.dto.request.SignupRequest;
import com.example.library.dto.response.AuthenticationResponse;
import com.example.library.dto.response.RefreshTokenResponse;
import com.example.library.exceptions.TokenRefreshException;
import com.example.library.model.RefreshToken;
import com.example.library.model.Role;
import com.example.library.model.User;
import com.example.library.repo.RoleRepository;
import com.example.library.repo.UserRepository;
import com.example.library.security.RefreshTokenService;
import com.example.library.security.UserDetailsImpl;
import com.example.library.security.UserDetailsServiceImpl;
import com.example.library.security.jwt.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;


//@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600, allowCredentials="true")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    private Environment environment;

    @GetMapping("/{id}")
    public ResponseEntity<Object> getUserByID(@PathVariable UUID id) {

        Optional<User> user = userDetailsService.getFaqsByID(id);
        return new ResponseEntity<>(user, HttpStatus.OK);

    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUserEmail(), loginRequest.getPassword()));
        logger.info(authentication.toString());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());

        ResponseCookie jwtRefreshCookie = jwtUtils.generateRefreshJwtCookie(refreshToken.getToken());

        String jwt = jwtUtils.getCJwt();
        String jwtRefresh = refreshToken.getToken();


        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                .header(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString())
                .body(new AuthenticationResponse("Logged in ",
                        HttpStatus.OK.value(),
                        HttpStatus.OK.toString(),
                        userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getFirstName(),
                        userDetails.getLastName(),
                        userDetails.getEmail(),
                        userDetails.getPhoneNumber(),
                        userDetails.getBirthDate(),
                        roles,
                        jwt,
                        jwtRefresh)
                );


    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUser_name())) {
            return ResponseEntity.badRequest().body(new Response("Error: Username is already taken!", HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value()));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new Response("Error: Email is already in use!", HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value()));
        }

        // Create new user's account
        var user = User.builder()
                .first_name(signUpRequest.getFirst_name())
                .last_name(signUpRequest.getLast_name())
                .username(signUpRequest.getUser_name())
                .email(signUpRequest.getEmail())
                .password(encoder.encode(signUpRequest.getPassword()))
                .phone_number(signUpRequest.getPhone_number())
                .date_of_birth(signUpRequest.getDate_of_birth())
                .build();

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(RoleType.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(RoleType.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(RoleType.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(RoleType.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        List<String> r = roles.stream().map(item -> item.getName().toString()).collect(Collectors.toList());

        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(user);

        String jwt = jwtUtils.getCJwt();
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(user.getId());
        ResponseCookie jwtRefreshCookie = jwtUtils.generateRefreshJwtCookie(refreshToken.getToken());
        String jwtRefresh = refreshToken.getToken();
        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                .header(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString())
                .body(new AuthenticationResponse("successfully registered",
                        HttpStatus.OK.value(),
                        HttpStatus.OK.toString(),
                        user.getId(),
                        user.getUsername(),
                        user.getFirst_name(),
                        user.getLast_name(),
                        user.getEmail(),
                        user.getPhone_number(),
                        user.getDate_of_birth(),
                        r,
                        jwt
                        , jwtRefresh)

                );

    }

    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        Object principle = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        logger.info(principle.toString());
        if (!Objects.equals(principle.toString(), "anonymousUser")) {
            UUID userId = ((UserDetailsImpl) principle).getId();
            refreshTokenService.deleteByUserId(userId);
            ResponseCookie jwtCookie = jwtUtils.getCleanJwtCookie();
            ResponseCookie jwtRefreshCookie = jwtUtils.getCleanJwtRefreshCookie();

            return ResponseEntity.ok()
                    .header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                    .header(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString())
                    .body(new Response("You've been signed out!", HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value()));
        } else {
            return ResponseEntity.badRequest().body(new Response("U must sign in First !!!", HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value()));
        }


    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(HttpServletRequest request) {
        String refreshToken = jwtUtils.getJwtRefreshFromCookies(request);
        if (refreshToken == null) {
            refreshToken = request.getHeader("refreshToken");
        }

        if ((refreshToken != null) && (refreshToken.length() > 0)) {
            String finalRefreshToken = refreshToken;
            return refreshTokenService.findByToken(refreshToken)
                    .map(refreshTokenService::verifyExpiration)
                    .map(RefreshToken::getUser)
                    .map(user -> {
                        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(user);

                        return ResponseEntity.ok()
                                .header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                                .body(new RefreshTokenResponse("Token is refreshed successfully! ", jwtUtils.getCJwt(), HttpStatus.OK.getReasonPhrase(), HttpStatus.OK.value()));
                    })
                    .orElseThrow(() -> new TokenRefreshException(finalRefreshToken,
                            "Refresh token is not in database!"));
        }

        return ResponseEntity.badRequest().body(new Response("Refresh Token is empty!", HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value()));
    }

    @GetMapping("/port")
    public String getPort() {
        return "port is " + environment.getProperty("local.server.port");
    }
}