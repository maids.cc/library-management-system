package com.example.library.controller;


import com.example.library.allResponse.Response;
import com.example.library.model.Book;
import com.example.library.model.BorrowingRecord;
import com.example.library.model.Patron;
import com.example.library.service.BookService;
import com.example.library.service.BorrowingRecordService;
import com.example.library.service.PatronService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class BorrowingRecordController {

    @Autowired
    private BorrowingRecordService borrowingRecordService;
    @Autowired
    private BookService bookService;
    @Autowired
    private PatronService patronService;

    @PostMapping("/borrow/{bookId}/patron/{patronId}")
    @Transactional
    public ResponseEntity<Object> addBorrowingRecord(@PathVariable Long bookId, @PathVariable Long patronId, @Valid @RequestBody BorrowingRecord borrowingRecord) {
        if (borrowingRecord.getId() == null) {
            if (checkBorrowingAvailable(bookId)) { // check if book is available to borrow.

                Book book = bookService.getBookByID(bookId);
                Patron patron = patronService.getPatronByID(patronId);
                borrowingRecord.setBook_id(book);
                borrowingRecord.setPatron_id(patron);

                BorrowingRecord borrowingRecord1 = borrowingRecordService.addBorrowingRecord(borrowingRecord);
                Response response = new Response("add borrow was success", HttpStatus.CREATED.toString(), borrowingRecord1);
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                Response response = new Response("the book is still borrowing", HttpStatus.BAD_REQUEST.toString(), null);
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } else {
            Response response = new Response("add borrow is fault", HttpStatus.BAD_REQUEST.toString(), null);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    @PutMapping("/return/{bookId}/patron/{patronId}")
    @Transactional
    public ResponseEntity<Object> updateBorrowingRecord(@PathVariable Long bookId, @PathVariable Long patronId, @Valid @RequestBody BorrowingRecord borrowingRecord) {
        try {
            Optional<BorrowingRecord> borrowingRecord1 = Optional.ofNullable(this.borrowingRecordService.getBorrowingRecordByID(borrowingRecord.getId()));
            if (borrowingRecord1.isPresent()) {

                Book book = bookService.getBookByID(bookId);
                Patron patron = patronService.getPatronByID(patronId);
                borrowingRecord.setBook_id(book);
                borrowingRecord.setPatron_id(patron);

                borrowingRecord = borrowingRecordService.updateBorrowingRecord(borrowingRecord);
                Response response = new Response("update borrow record was success", HttpStatus.OK.toString(), borrowingRecord);
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
            Response response = new Response("update borrow is fault", HttpStatus.CREATED.toString(), null);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            Response response = new Response("update borrow is fault", HttpStatus.INTERNAL_SERVER_ERROR.toString(), null);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public boolean checkBorrowingAvailable(Long bookId) {
        return borrowingRecordService.checkBorrowBookAvailable(bookId);
    }


}
