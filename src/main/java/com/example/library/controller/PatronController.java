package com.example.library.controller;

import com.example.library.allResponse.Response;
import com.example.library.model.Patron;
import com.example.library.service.PatronService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/patrons")
public class PatronController {

    @Autowired
    private PatronService patronService;

    @GetMapping("/{id}")
    @Cacheable("patrons")
    public ResponseEntity<Object> getPatronByID(@PathVariable Long id) {
        Patron Patron = patronService.getPatronByID(id);
        Response response = new Response("get this list is success", HttpStatus.OK.toString(), Patron);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("")
    public ResponseEntity<Object> getAllPatrons() {
        List<Patron> Patron = patronService.getAllPatrons();
        Response response = new Response("get this list is success", HttpStatus.OK.toString(), Patron);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @PostMapping("/")
    @Transactional
    public ResponseEntity<Object> addPatron(@Valid @RequestBody Patron newPatron) {

        if (newPatron.getId() == null) {
            Patron Patron = patronService.addPatron(newPatron);
            Response response = new Response("add Patron was success", HttpStatus.CREATED.toString(), Patron);
            return new ResponseEntity<>(response, HttpStatus.OK);

        }
        Response response = new Response("add Patron  is fault", HttpStatus.BAD_REQUEST.toString(), null);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<Object> updatePatron(@PathVariable Long id, @Valid @RequestBody Patron newPatron) {
        patronService.getPatronByID(id);
        newPatron.setId(id);
        newPatron = patronService.updatePatron(newPatron);
        Response response = new Response("update Patron was success", HttpStatus.OK.toString(), newPatron);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletePatron(@PathVariable Long id) {
        patronService.getPatronByID(id);
        this.patronService.deletePatron(id);
        Response response = new Response(" delete Patron was success " + id, HttpStatus.OK.toString(), null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}