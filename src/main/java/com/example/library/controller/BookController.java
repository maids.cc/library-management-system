package com.example.library.controller;

import com.example.library.allResponse.Response;
import com.example.library.model.Book;
import com.example.library.repo.BookRepo;
import com.example.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private BookRepo bookRepo;


    @GetMapping("/{id}")
    @Cacheable("books")
    public ResponseEntity<Object> getBookByID(@PathVariable Long id) {
        Book Book = bookService.getBookByID(id);
        Response response = new Response("get this list is success", HttpStatus.OK.toString(), Book);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("")
    public ResponseEntity<Object> getAllBooks() {
        List<Book> Book = bookService.getAllBooks();
        Response response = new Response("get this list is success", HttpStatus.OK.toString(), Book);
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @PostMapping("/")
    @Transactional
    public ResponseEntity<Object> addBook(@Valid @RequestBody Book newBook) {

        if (newBook.getId() == null) {
            Book Book = bookService.addBook(newBook);
            Response response = new Response("add Book was success", HttpStatus.CREATED.toString(), Book);
            return new ResponseEntity<>(response, HttpStatus.OK);

        }
        Response response = new Response("add Book  is fault", HttpStatus.BAD_REQUEST.toString(), null);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<Object> updateBook(@PathVariable Long id, @Valid @RequestBody Book newBook) {
        this.bookService.getBookByID(id);
        newBook.setId(id);
        newBook = bookService.updateBook(newBook);
        Response response = new Response("update Book was success", HttpStatus.OK.toString(), newBook);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteBook(@PathVariable Long id) {
        this.bookService.getBookByID(id);
        this.bookService.deleteBook(id);
        Response response = new Response(" delete Book was success " + id, HttpStatus.OK.toString(), null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}