package com.example.library.repo;

import com.example.library.model.BorrowingRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BorrowingRecordRepo extends JpaRepository<BorrowingRecord, Long> {

    @Query(value = "SELECT update_date FROM borrowing_record WHERE book_id = :id ORDER BY borrow_date desc LIMIT 1 ", nativeQuery = true)
    Object getLastRecordForBookId(@Param("id") Long BookId);

    @Query(value = "SELECT * from borrowing_record  WHERE book_id = :id", nativeQuery = true)
    Optional<BorrowingRecord> getRecordsByBookId(@Param("id") Long BookId);
}
