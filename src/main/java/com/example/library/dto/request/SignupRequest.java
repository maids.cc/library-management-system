package com.example.library.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SignupRequest {


    @NotBlank
    private String first_name;

    @NotBlank
    private String last_name;

    @NotBlank
    @Size(min = 3, max = 20)
    private String user_name;
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;


    private Set<String> role;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    @NotBlank
    private String phone_number;



    @JsonFormat(pattern="yyyy/MM/dd")
    private LocalDate date_of_birth;
  

}
