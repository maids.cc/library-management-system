package com.example.library.dto.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RefreshTokenResponse

{
    private String message;
    private String Token;
    private String Reason;
    private int StatusCode;

}
