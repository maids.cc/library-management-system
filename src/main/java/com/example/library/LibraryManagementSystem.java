package com.example.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@SpringBootApplication
@EnableJpaAuditing
public class LibraryManagementSystem {

    public static void main(String[] args) {
        SpringApplication.run(LibraryManagementSystem.class, args);
    }
}
