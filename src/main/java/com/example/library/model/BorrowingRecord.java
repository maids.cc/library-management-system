package com.example.library.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name="BorrowingRecord")
public class BorrowingRecord extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JoinColumn(name = "id", nullable = false)
    private Long id;

    @JoinColumn(name = "borrowing")
    @NotEmpty(message = "borrowing info should not be empty")
    private String borrowing;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book_id;

    @ManyToOne
    @JoinColumn(name = "patron_id")
    private Patron patron_id;

    @PrePersist // set returned_date null when post new record
    protected void onCreate()
    {
        setUpdate_date(null);
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBorrowing() {
        return borrowing;
    }

    public void setBorrowing(String borrowing) {
        this.borrowing = borrowing;
    }

    public void setBook_id(Book book_id) {
        this.book_id = book_id;
    }

    public Patron getPatron_id() {
        return patron_id;
    }

    public void setPatron_id(Patron patron_id) {
        this.patron_id = patron_id;
    }

    public Book getBook_id() {
        return book_id;
    }

}
