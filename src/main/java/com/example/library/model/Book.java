package com.example.library.model;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Book")
public class Book extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @JoinColumn(name = "title")
    @NotEmpty(message = "title should not be null !")
    private String title;
    @JoinColumn(name = "author")
    @NotEmpty(message = "author name should not be empty")
    private String author;
    @JoinColumn(name = "publication_year")
    @NotEmpty(message = "publication_year  should not be empty")
    private String publication_year;
    @JoinColumn(name = "ISBN")
    @NotEmpty(message = "ISBN  should not be empty")
    private String isbn;

//    @JsonManagedReference
//    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
//    @OneToMany(mappedBy = "id", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    private BorrowingRecord borrowingRecord;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublication_year() {
        return publication_year;
    }

    public void setPublication_year(String publication_year) {
        this.publication_year = publication_year;
    }

    public String getISBN() {
        return isbn;
    }

    public void setISBN(String isbn) {
        this.isbn = isbn;
    }
}
