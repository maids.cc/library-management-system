package com.example.library.constants;

public enum RoleType {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN
}