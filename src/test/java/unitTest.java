import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.library.model.Book;
import com.example.library.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class unitTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private BookService bookService;

    @Test
    public void testCreateBook() throws Exception {
        Book book = new Book(1L,"kk","ff","dd","dd");

        when(bookService.addBook(book)).thenReturn(book);

        mockMvc.perform(post("/api/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(book)))
                .andExpect(status().isOk());

        verify(bookService, times(1)).addBook(book);
    }

    @Test
    public void testGetBook() throws Exception {
        Book book = new Book(1L,"kk","ff","dd","dd");

        when(bookService.getBookByID(1L)).thenReturn(book);

        mockMvc.perform(get("/api/books/1"))
                .andExpect(status().isOk());

        verify(bookService, times(1)).getBookByID(1L);
    }

    @Test
    public void testUpdateBook() throws Exception {
        Book book = new Book(1L,"kk","ff","dd","dd");

        when(bookService.updateBook(book)).thenReturn(book);

        mockMvc.perform(put("/api/books/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(book)))
                .andExpect(status().isOk());

        verify(bookService, times(1)).updateBook(book);
    }

    @Test
    public void testDeleteBook() throws Exception {
        Long bookId = 1L;

        mockMvc.perform(delete("/api/books/1"))
                .andExpect(status().isOk());

        verify(bookService, times(1)).deleteBook(bookId);
    }
}